<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<title>Soal Test PT. Qtasnim Digital Teknologi</title>
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">
	<style>
		.loader {
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			background-color: #F5F8FA;
			z-index: 9998;
			text-align: center;
		}

		.plane-container {
			position: absolute;
			top: 50%;
			left: 50%;
		}
	</style>
</head>
<body class="light">
	<!-- Pre loader -->
	<div id="loader" class="loader">
		<div class="plane-container">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div><div class="gap-patch">
						<div class="circle"></div>
					</div><div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

				<div class="spinner-layer spinner-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div><div class="gap-patch">
						<div class="circle"></div>
					</div><div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

				<div class="spinner-layer spinner-yellow">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div><div class="gap-patch">
						<div class="circle"></div>
					</div><div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

				<div class="spinner-layer spinner-green">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div><div class="gap-patch">
						<div class="circle"></div>
					</div><div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="app">
		<main>
			<div id="primary" class="p-t-b-100 height-full ">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 mx-md-auto">
							<div class="text-center">
								<img src="logo.png" alt="" width="114px">
								<?php
								require(__DIR__ . '/../modules/config.php');
								if(@$_GET['login']=='fail'):
									echo '<div class="toast"
									data-title="Login Gagal!"
									data-message="Username/Password Anda Salah!"
									data-type="error">
									</div>';
								endif;
								?>
								<p class="p-t-b-10">Soal Test</p>
								<h3 class="mb-2">PT. Qtasnim Digital Teknologi</h3>
							</div>
							<form id="checkLogin" >
								<?php echo csrf_field() ?>
								<div class="form-group has-icon"><i class="icon-envelope-o"></i>
									<input type="text" name="username" class="form-control form-control-lg"
									placeholder="Username Anda">
								</div>
								<div class="form-group has-icon"><i class="icon-user-secret"></i>
									<input type="password" name="password" class="form-control form-control-lg"
									placeholder="Password Anda">
								</div>
								<input type="submit" class="btn btn-success btn-lg btn-block" value="Log In">
								<br>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- #primary -->
		</main>
		<div class="control-sidebar-bg shadow white fixed"></div>
	</div>
	<!--/#app -->
	<script type="text/javascript">
    // Anonymous Function
    (function(){
      // Menghilangkan Loader Setelah 1,5Detik
      setTimeout(function() {
      	var x = document.getElementById("loader");
      	x.style.display = "none";
      }, 1500);
      // Download JS Setelah 0,05detik Di Background
      setTimeout(function() {
      	var a = document.createElement('script');
      	a.type = 'text/javascript';
      	a.async = true;
      	a.src = 'assets/js/app.js';
      	var b = document.getElementsByTagName('script')[0];
      	b.parentNode.insertBefore(a, b);
      }, 50);

      const postData = async function(url = '', data = {}) {

      	var form_data = new FormData();

      	for ( var key in data ) {
      		form_data.append(key, data[key]);
      	}

      	const response = await fetch(url, {
      		method: 'POST',
      		body: form_data
      	});
      	return response.json();
      }

      const input = (name) => {
      	let data = document.getElementsByName(name);
      	return data[0].value;
      }

      document.getElementById("checkLogin").onsubmit = function(e) {

      	e.preventDefault();

      	postData('auth.php?p=login', { _token : input('_token'), username : input('username'), password : input('password'), type : 'api' }).then(data => {
      		console.log("data", data);
        	// input('_token') = data.csrf_token;
        	if (!!data.error) {
        		location.href='index.php?login=fail';
        	} else {
        		window.localStorage.setItem('access_token', data.data.access_token);
        		window.localStorage.setItem('refresh_token', data.data.refresh_token);

        		location.href='home.php';
        	}

        });

      }

      let access_token = window.localStorage.getItem('access_token');
      let refresh_token = window.localStorage.getItem('refresh_token');

      if (access_token || refresh_token) {
      	postData('auth.php?p=checkLogin', { access_token : access_token, type : 'api' }).then(data => {
      		if (!!data.error) {
      			postData('auth.php?p=refreshToken', { refresh_token : refresh_token, type : 'api' }).then(data => {
      				if (!!data.error) {
      					window.localStorage.removeItem('access_token');
      					window.localStorage.removeItem('refresh_token');
      				} else {
      					location.href='home.php';
      				}
      			});
      		} else {
      			location.href='home.php';
      		}

      	});
      }

      function getSearchParameters() {
      	var prmstr = window.location.search.substr(1);
      	return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
      }

      function transformToAssocArray( prmstr ) {
      	var params = {};
      	var prmarr = prmstr.split("&");
      	for ( var i = 0; i < prmarr.length; i++) {
      		var tmparr = prmarr[i].split("=");
      		params[tmparr[0]] = tmparr[1];
      	}
      	return params;
      }

      let url_path = getSearchParameters();
      if (url_path.destroy_token == 'yes'){
      	window.localStorage.removeItem('access_token');
      	window.localStorage.removeItem('refresh_token');
      }
  })();
</script>
</body>
</html>