<?php
// error_reporting(0);

date_default_timezone_set('Asia/Jakarta');
require(__DIR__ . '/../modules/config.php');

// get parameter halaman dinamis
$page = @$_GET['p'];

if (@$_SESSION['IsLogin']!=true) {
	echo "<script>location.href='index.php?destroy_token=yes';</script>";
	exit();
} else if (empty($page)) {
	echo "<script>location.href='home.php?p=dashboard'</script>";
	exit();
}

// halaman dinamis berdasarkan parameter q
if (!(in_array($page, $allPages['web']))) {
	// jika tidak ada didalam list semua_halaman
	require(__DIR__ . "/../modules/_web/_error/404.php");
} else if (in_array($page, $acl['web'][@$_SESSION['Level']]) && preg_match("/^[a-zA-Z\/_-]*$/",$page) ) {
	require(__DIR__ . "/../modules/_web/_layout/header.php");
	require(__DIR__ . "/../modules/_web/_layout/menu.php");
	require(__DIR__ . "/../modules/_web/".$page.".php");
	require(__DIR__ . "/../modules/_web/_layout/footer.php");
} else {
	// jika tidak memiliki akses
	require(__DIR__ . "/../modules/_web/_error/403.php");
}