<?php
// error_reporting(0);
try{
	require(__DIR__ . '/../modules/config.php');

	$level = @$_SESSION['Level'] ? @$_SESSION['Level'] : '';

	header('Content-Type: application/json');

	// get parameter halaman dinamis
	$page = isset($_GET['p']) ? $_GET['p'] : '';
	$act  = isset($_GET['act']) ? $_GET['act'] : '';
	$q    = isset($_GET['q']) ? $_GET['q'] : '';

	$validation = $v->validate($_POST + $_GET, [
		'access_token'	=> 'required',
	]);

	if ($isValidToken && !$validation->fails()) {
		try {
			$data = $jwt->decode(@$_POST['access_token'] ? @$_POST['access_token'] : @$_GET['access_token'], ACCESS_TOKEN_SECRET, ['HS256']);
			$level = $data->users_type;
		} catch (Exception $e) {
			echo json_encode([
				'error'        => true,
				'messages'     => "Invalid Token!",
				'data'         => $e->getMessage(),
				'csrf_token'   => csrf_token(),
			]);
			header("location: index.php", true, 301);
			exit();
		}
	} else {
		if (@$_SESSION['IsLogin']!=true) {
			echo json_encode([
				'error'        => true,
				'messages'     => "Invalid Session!",
				'data'         => null,
				'csrf_token'   => csrf_token(),
			]);
			header("location: index.php", true, 301);
			exit();
		} else if (empty($page)) {
			echo response_error(null, '404 Not Found');
			header("location: index.php", true, 301);
			exit();
		}
	}

	// halaman dinamis berdasarkan parameter q
	if (!(in_array($page, $allPages['api']))) {

		echo response_error(null, '404 Not Found');
		header("location: index.php", true, 301);
		exit();

	} else if (in_array($page, $acl['api'][$level]) && preg_match("/^[a-zA-Z\/_-]*$/",$page) ) {

		// Memanggil Library Server Side Rendering DataTables
		require(__DIR__ . "/../modules/ssp.class.php");

		require(__DIR__ . "/../modules/_api/".$page.".php");

		exit();
	} else {

		// jika tidak memiliki akses
		echo response_error(null, '403 Forbidden');
		header("location: index.php", true, 301);
		exit();
	}

} catch(PDOException $e) {

	// echo $e->getMessage();
	echo response_error([
		'validator' => $validData,
		'exception' => $e->getMessage()
	], 'Terjadi Kesalahan!');
	http_response_code(401);
	exit();
}