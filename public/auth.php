<?php
// error_reporting(0);

require(__DIR__ . '/../modules/config.php');

switch (@$_GET['p']) {
	// Page Login
	case 'login':
	require(__DIR__ . "/../modules/_auth/login.php");
	break;
	// Page checkLogin
	case 'checkLogin':
	require(__DIR__ . "/../modules/_auth/checkLogin.php");
	break;
	// Page refreshToken
	case 'refreshToken':
	require(__DIR__ . "/../modules/_auth/refreshToken.php");
	break;
	// Page Logout
	case 'logout':
	require(__DIR__ . "/../modules/_auth/logout.php");
	break;
	// Default Case Direct To index.php
	default:
	echo response_error();
	http_response_code(401);
	exit();
	break;
}

echo response_error();
http_response_code(401);
exit();