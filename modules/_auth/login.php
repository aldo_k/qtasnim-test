<?php

$validation = $v->validate($_POST + $_FILES, [
	'username'	=> 'required',
	'password'	=> 'required',
]);

if ($isValidToken && !$validation->fails()) {
	$records = $db->table('users')
	->select('*')
	->where('username', '=', @$_POST['username'])
	->get();

	if(@$records->password===md5(@$_POST['password'])){

		$_SESSION['UserID']    = $records->users_id;
		$_SESSION['Fullname']  = $records->fullname;
		$_SESSION['Username']  = $records->username;
		$_SESSION['Level']     = $records->users_type;
		$_SESSION['Reference'] = $records->ref_id;
		$_SESSION['IsLogin']   = true;

		// 60 * 60 (detik) = 60 menit
		$expired_time = time() + (60 * 60);

		$payload_token = [
			'user_id' => $records->users_id,
			'username' => $records->username,
			'users_type' => $records->users_type,
			'exp' => $expired_time
		];

		$access_token = $jwt->encode($payload_token, ACCESS_TOKEN_SECRET);

		// 7 * 24 * 60 * 60 (detik) = 24 Jam * 7
		$payload_token['exp'] = time() + (7 * 24 * 60 * 60);
		$refresh_token = $jwt->encode($payload_token, REFRESH_TOKEN_SECRET);

		echo json_encode([
			'error' => false,
			'messages' => "Login Successfuly!",
			'data' => [
				'access_token'  => $access_token,
				'refresh_token' => $refresh_token,
				'expiry'        => date(DATE_ISO8601, $expired_time)
			],
			'csrf_token' => csrf_token(),
		]);
		exit();
		// echo "<script>location.href='halaman.php'</script>";
	} else {
		echo json_encode([
			'error'      => true,
			'messages'   => "Wrong Username/Password!",
			'data'       => null,
			'csrf_token' => csrf_token(),
		]);
		exit();
	}
}