<?php

$validation = $v->validate($_POST, [
	'refresh_token'	=> 'required',
]);

if ($isValidToken && !$validation->fails()) {
	try {

		$data = $jwt->decode(@$_POST['refresh_token'], REFRESH_TOKEN_SECRET, ['HS256']);

		$records = $db->table('users')
		->select('*')
		->where('username', '=', $data->username)
		->get();

		$_SESSION['UserID']    = $records->users_id;
		$_SESSION['Fullname']  = $records->fullname;
		$_SESSION['Username']  = $records->username;
		$_SESSION['Level']     = $records->users_type;
		$_SESSION['Reference'] = $records->ref_id;
		$_SESSION['IsLogin']   = true;

		// 60 * 60 (detik) = 60 menit
		$expired_time = time() + (60 * 60);

		$payload_token = [
			'user_id'    => $records->users_id,
			'username'   => $records->username,
			'users_type' => $records->users_type,
			'exp'        => $expired_time
		];

		$access_token = $jwt->encode($payload_token, ACCESS_TOKEN_SECRET);

		// 7 * 24 * 60 * 60 (detik) = 24 Jam * 7
		$payload_token['exp'] = time() + (7 * 24 * 60 * 60);
		$refresh_token = $jwt->encode($payload_token, REFRESH_TOKEN_SECRET);

		$data = [
			'access_token'  => $access_token,
			'refresh_token' => $refresh_token,
			'expiry'        => date(DATE_ISO8601, $expired_time)
		];

		echo response($data, "Re-Login Successfuly!");
		exit();

	} catch (Exception $e) {
		echo response_error($e->getMessage());
		http_response_code(401);
		exit();
	}
}