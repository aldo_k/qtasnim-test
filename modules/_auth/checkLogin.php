<?php

$validation = $v->validate($_POST, [
	'access_token'	=> 'required',
]);

if ($isValidToken && !$validation->fails()) {
	try {
		$data = $jwt->decode(@$_POST['access_token'], ACCESS_TOKEN_SECRET, ['HS256']);

		echo response([
			'access_token' => $_POST['access_token'],
			'expiry'       => date(DATE_ISO8601, $data->exp)
		]);
		exit();
	} catch (Exception $e) {
		echo response_error($e->getMessage());
		http_response_code(401);
		exit();
	}
}