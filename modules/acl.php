<?php
$allPages = [
	'web' => [
		"admin", "dashboard", "password", "transaction", "product", "product_type", "unit"
	],
	'api' => [
		"admin", "dashboard", "password", "transaction", "product", "product_type", "unit",
	],
];

// mendefinisikan halaman apa saja yang dapat diakses oleh pengawas
$acl = [
	'web' => [
		// Admin
		'1'=>[
			"admin", "dashboard", "password", "transaction", "product", "product_type", "unit",
		],

	],

	'api' => [
		// Admin
		'1'=>[
			"admin", "dashboard", "password", "transaction", "product", "product_type", "unit",
		],

	]
];
