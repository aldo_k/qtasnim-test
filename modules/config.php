<?php
require(__DIR__ . '/../vendor/autoload.php');

use Buki\Pdox;
use Firebase\JWT\JWT;
use Rakit\Validation\Validator;

if(!isset($_SESSION)) { session_start(); }

// Database Config For PDOx Library
$config = [
	'db' => [
		'host'		=> 'localhost',
		'driver'	=> 'mysql',
		'database'	=> 'qtasnim_test',
		'username'	=> 'root',
		'password'	=> '',
		'charset'	=> 'utf8',
		'collation'	=> 'utf8_general_ci',
		'prefix'	 => '',
	]
];

// https://github.com/izniburak/pdox
$db = new Pdox($config['db']);

// https://github.com/rakit/validation
$v 	= new Validator;

$v->setMessages([
	'required' => ':attribute harus diisi',
	'email' => ':email tidak valid',
	'same' => ':attribute tidak valid',
]);

$jwt = new JWT();

const ACCESS_TOKEN_SECRET = "9Ug+G0MOG9zkEdQf/kgo+Ayfh9v3hsDzp26WGpnVRDM=";
const REFRESH_TOKEN_SECRET = "3cv2GgMOGfzkf/Qw/kNo+Ayfo9vhfsDzpf6WGpnVRDM=";

// Access Control List (Listing Hak Akses)
require (__DIR__ . '/acl.php');
// Function (Kumpulan Fungsi Dasar)
require (__DIR__ . '/function.php');
