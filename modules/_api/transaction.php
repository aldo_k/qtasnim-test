<?php

$table      = 'transactions';
$primaryKey = 'id';
$joinQuery = "FROM `transactions` AS `trx` INNER JOIN `products` AS `p` ON `trx`.`product_id`=`p`.`id` INNER JOIN `product_types` AS `pt` ON `p`.`product_type_id`=`pt`.`id`";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*, trx.*, trx.id as p_id, p.id as product_id, pt.id as product_type_id";

$columns = [
	['tb' => 'trx', 'db' => 'id', 'as' => 'id', 'dt' => 'id'],
	['db' => 'product', 'dt' => 'product'],
	['db' => 'product_type', 'dt' => 'product_type'],
	['db' => 'stock_old', 'dt' => 'stock_old'],
	['db' => 'qty', 'dt' => 'qty'],
	['db' => 'transaction_date', 'dt' => 'transaction_date'],
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';

$validation = $v->make($_POST, [
	'product_id' => 'required',
	'qty'  => 'required',
	'transaction_date' => 'required',
]);

$validation->setAliases([
	'transaction_date' => 'Tanggal Transaksi Penjualan',
	'product_id' => 'Barang',
	'qty'  => 'Jumlah Barang',
]);

$validation->validate();

$validData = $validation->getValidData();

$message = [ 'form' => $validation->errors()->all() ];

$query = $db->table($table);

switch ($act) {
	case 'datatables':

	if ((int)@$_POST['product_type_id']>0) {
		$product_type_id = (int) $_POST['product_type_id'];
		if ($extraWhere=="") {
			$extraWhere .= "`p`.`product_type_id` = '$product_type_id' ";
		}
	}

	if ((int)@$_POST['product_id']>0) {
		$product_id = (int) $_POST['product_id'];
		if ($extraWhere=="") {
			$extraWhere .= "`trx`.`product_id` = '$product_id' ";
		} else {
			$extraWhere .= " AND `trx`.`product_id` = '$product_id' ";
		}
	}

	if (@$_POST['date_start'] && @$_POST['date_end']) {
		$date_start = date('Y-m-d', strtotime(@$_POST['date_start']));
		$date_end = date('Y-m-d', strtotime(@$_POST['date_end']));
		if ($extraWhere=="") {
			$extraWhere .= "`trx`.`transaction_date` BETWEEN '$date_start' AND '$date_end' ";
		} else {
			$extraWhere .= " AND `trx`.`transaction_date` BETWEEN '$date_start' AND '$date_end' ";
		}
	}

	echo json_encode(SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having));
	exit();
	break;

	case 'read':
	echo json_encode(
		$query->select('*, transactions.*, transactions.id as id')
		->join('products', 'transactions.product_id', 'products.id')
		->where('transactions.id', $id)
		->get()
	);
	exit();
	break;

	case 'select2':
	$select2 = $query->select('id, product as text')->like('product', "%{$q}%");
	echo json_encode([ "results" => $select2->getAll() ]);
	exit();
	break;

	case 'create':
	if($isValidToken && !$validation->fails())
	{
		$validData['qty'] = abs($validData['qty']);

		$product = $db->table('products')->select('*')->where('products.id', $validData['product_id'])->get();

		$validData['stock_old'] = $product->stock;

		$stock_now = $product->stock - $validData['qty'];
		if ($stock_now <= 0) {
			echo response_error(null, 'Stok Tidak Cukup!');
			exit();
		}

		$db->table($table)->insert($validData);

		$db->table('products')->where('products.id', $validData['product_id'])->update(['stock' => $stock_now ]);

	}
	break;

	case 'update':
	if($isValidToken && !$validation->fails()){

		$validData['qty'] = abs($validData['qty']);

		$transaction = $query->where($primaryKey, $id)->get();

		$product     = $db->table('products')->select('*')->where('products.id', $validData['product_id'])->get();
		$qty_old     = $transaction->qty - $validData['qty'];

		$stock_now = $product->stock + $qty_old;
		if ($stock_now <= 0) {
			echo response_error(null, 'Stok Tidak Cukup!');
			exit();
		}

		$db->table($table)->where($primaryKey, $id)->update($validData);

		$db->table('products')->where('products.id', $validData['product_id'])->update(['stock' => $stock_now ]);
	}
	break;

	case 'delete':

	$transaction = $query->where($primaryKey, $id)->get();
	$product     = $db->table('products')->select('*')->where('products.id', $transaction->product_id)->get();
	$stock_now   = $product->stock + $transaction->qty;

	$db->table($table)->where($primaryKey, $id)->delete();

	$db->table('products')->where('products.id', $transaction->product_id)->update(['stock' => $stock_now ]);

	$message = "Berhasil Dihapus";
	break;

	default:
	echo response_error(null, '404 Not Found');
	header("location: index.php", true, 301);
	exit();
	break;
}

echo response($validData, $message);
exit();