<?php

$table      = 'products';
$primaryKey = 'id';
$joinQuery = "FROM `products` AS `p` INNER JOIN `units` AS `u` ON `p`.`unit_id`=`u`.`id` INNER JOIN `product_types` AS `pt` ON `p`.`product_type_id`=`pt`.`id`";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*, p.*, p.id as p_id, u.id as unit_id, pt.id as product_type_id";

$columns = [
	['tb' => 'p', 'db' => 'id', 'as' => 'id', 'dt' => 'id'],
	['db' => 'product', 'dt' => 'product'],
	['db' => 'descriptions', 'dt' => 'descriptions'],
	['db' => 'stock', 'dt' => 'stock'],
	['db' => 'stock_alert', 'dt' => 'stock_alert'],
	['db' => 'product_type', 'dt' => 'product_type'],
	['db' => 'unit', 'dt' => 'unit'],
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';

$validation = $v->make($_POST, [
	'product'  => 'required|min:3',
	'stock' => 'nullable',
	'stock_alert' => 'nullable',
	'product_type_id' => 'required',
	'unit_id' => 'required',
	'descriptions' => 'nullable',
]);

$validation->setAliases([
	'product'  => 'Nama Barang',
	'stock' => 'Stock Barang',
	'stock_alert' => 'Peringatan Stok Barang',
	'product_type_id' => 'Jenis Barang',
	'unit_id' => 'Satuan',
	'descriptions' => 'Deskripsi',
]);

$validation->validate();

$validData = $validation->getValidData();

$message = [ 'form' => $validation->errors()->all() ];

$query = $db->table($table);

switch ($act) {
	case 'datatables':
	echo json_encode(SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having));
	exit();
	break;

	case 'read':
	echo json_encode(
		$query->select('*, products.*, products.id as id, units.id as unit_id, product_types.id as product_type_id')
		->join('units', 'products.unit_id', 'units.id')
		->join('product_types', 'products.product_type_id', 'product_types.id')
		->where('products.id', $id)
		->get()
	);
	exit();
	break;

	case 'select2':

	$type_id = isset($_GET['product_type_id']) ? $_GET['product_type_id'] : '';
	$select2 = $query->select('id, product as text')->like('product', "%{$q}%");

	if ($type_id) {
		$select2 = $select2->where('product_type_id', $type_id);
	}

	echo json_encode([ "results" => $select2->getAll() ]);

	exit();
	break;

	case 'create':
	if($isValidToken && !$validation->fails())
		$query->insert($validData);
	break;

	case 'update':
	if($isValidToken && !$validation->fails())
		$query->where($primaryKey, $id)->update($validData);
	break;

	case 'delete':
	$query->where($primaryKey, $id)->delete();
	$message = "Berhasil Dihapus";
	break;

	default:
	echo response_error(null, '404 Not Found');
	header("location: index.php", true, 301);
	exit();
	break;
}

echo response($validData, $message);
exit();