<?php

$table      = 'product_types';
$primaryKey = 'id';
$joinQuery  = "";
$extraWhere = "";
$groupBy    = "";
$having     = "";
$select     = "*";

$columns = [
	['db' => 'id', 'dt' => 'id'],
	['db' => 'alias', 'dt' => 'alias'],
	['db' => 'product_type', 'dt' => 'product_type'],
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';

$validation = $v->make($_POST, [
	'alias' => 'required',
	'product_type'  => 'required|min:3',
]);

$validation->setAliases([
	'alias' => 'Alias Satuan',
	'product_type'  => 'Nama Satuan',
]);

$validation->validate();

$validData = $validation->getValidData();

$message = [ 'form' => $validation->errors()->all() ];

$query = $db->table($table);

switch ($act) {
	case 'datatables':
	echo json_encode(SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having));
	exit();
	break;

	case 'read':
	echo json_encode($query->select($select)->where($primaryKey,$id)->get());
	exit();
	break;

	case 'select2':
	echo json_encode([
		"results" => $query->select('id, product_type as text')
		->like('alias', "%{$q}%")
		->like('product_type', "%{$q}%")
		->getAll()
	]);
	exit();
	break;

	case 'create':
	if($isValidToken && !$validation->fails())
		$query->insert($validData);
	break;

	case 'update':
	if($isValidToken && !$validation->fails())
		$query->where($primaryKey, $id)->update($validData);
	break;

	case 'delete':
	$query->where($primaryKey, $id)->delete();
	$message = "Berhasil Dihapus";
	break;

	default:
	echo response_error(null, '404 Not Found');
	header("location: index.php", true, 301);
	exit();
	break;
}

echo response($validData, $message);
exit();