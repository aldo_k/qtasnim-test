<?php
$status = true;

$table = 'attendance';
$primaryKey = 'attendance_id';
$joinQuery = "FROM attendance RIGHT JOIN student USING(`student_id`) LEFT JOIN class ON `student`.`class_id`=`class`.`class_id`";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*";

if ((int)@$_POST['class_id']>0) {
	$class_id = (int) $_POST['class_id'];
	$extraWhere .= "`attendance`.`class_id` = '$class_id' ";
} else if (@$_POST['period_id']=="all" && $_SESSION['Level']!=1) {
	$extraWhere .= "`attendance`.`class_id` = '' ";
}

if ((int)@$_POST['period_id']>0) {
	$period_id = (int) $_POST['period_id'];
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`period_id` = '$period_id' ";
	} else {
		$extraWhere .= " AND `attendance`.`period_id` = '$period_id' ";
	}
}  else if (@$_POST['period_id']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`period_id` = '' ";
	} else {
		$extraWhere .= " AND `attendance`.`period_id` = '' ";
	}
}

if ((int)@$_POST['attendance_year']>0) {
	$attendance_year = (int) $_POST['attendance_year'];
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`attendance_year` = '$attendance_year' ";
	} else {
		$extraWhere .= " AND `attendance`.`attendance_year` = '$attendance_year' ";
	}
} else if (@$_POST['attendance_year']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`attendance_year` = '' ";
	} else {
		$extraWhere .= " AND `attendance`.`attendance_year` = '' ";
	}
}

if ((int)@$_POST['attendance_month']>0) {
	$attendance_month = sprintf('%02s', (int) $_POST['attendance_month']);
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`attendance_month` = '$attendance_month' ";
	} else {
		$extraWhere .= " AND `attendance`.`attendance_month` = '$attendance_month' ";
	}
} else if (@$_POST['attendance_month']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`attendance`.`attendance_month` = '' ";
	} else {
		$extraWhere .= " AND `attendance`.`attendance_month` = ''  ";
	}
}

// echo $extraWhere;

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'attendance_id', 'dt' => 'attendance_id'],
	['db' => 'attendance_year', 'dt' => 'attendance_year'],
	['db' => 'attendance_month', 'dt' => 'attendance_month'],
	['db' => 'student_id', 'dt' => 'student_id'],
	['db' => 'attendance_count_s', 'dt' => 'attendance_count_s'],
	['db' => 'attendance_count_i', 'dt' => 'attendance_count_i'],
	['db' => 'attendance_count_a', 'dt' => 'attendance_count_a'],
	['db' => 'attendance_day01', 'dt' => 'attendance_day01'],
	['db' => 'attendance_day02', 'dt' => 'attendance_day02'],
	['db' => 'attendance_day03', 'dt' => 'attendance_day03'],
	['db' => 'attendance_day04', 'dt' => 'attendance_day04'],
	['db' => 'attendance_day05', 'dt' => 'attendance_day05'],
	['db' => 'attendance_day06', 'dt' => 'attendance_day06'],
	['db' => 'attendance_day07', 'dt' => 'attendance_day07'],
	['db' => 'attendance_day08', 'dt' => 'attendance_day08'],
	['db' => 'attendance_day09', 'dt' => 'attendance_day09'],
	['db' => 'attendance_day10', 'dt' => 'attendance_day10'],
	['db' => 'attendance_day11', 'dt' => 'attendance_day11'],
	['db' => 'attendance_day12', 'dt' => 'attendance_day12'],
	['db' => 'attendance_day13', 'dt' => 'attendance_day13'],
	['db' => 'attendance_day14', 'dt' => 'attendance_day14'],
	['db' => 'attendance_day15', 'dt' => 'attendance_day15'],
	['db' => 'attendance_day16', 'dt' => 'attendance_day16'],
	['db' => 'attendance_day17', 'dt' => 'attendance_day17'],
	['db' => 'attendance_day18', 'dt' => 'attendance_day18'],
	['db' => 'attendance_day19', 'dt' => 'attendance_day19'],
	['db' => 'attendance_day20', 'dt' => 'attendance_day20'],
	['db' => 'attendance_day21', 'dt' => 'attendance_day21'],
	['db' => 'attendance_day22', 'dt' => 'attendance_day22'],
	['db' => 'attendance_day23', 'dt' => 'attendance_day23'],
	['db' => 'attendance_day24', 'dt' => 'attendance_day24'],
	['db' => 'attendance_day25', 'dt' => 'attendance_day25'],
	['db' => 'attendance_day26', 'dt' => 'attendance_day26'],
	['db' => 'attendance_day27', 'dt' => 'attendance_day27'],
	['db' => 'attendance_day28', 'dt' => 'attendance_day28'],
	['db' => 'attendance_day29', 'dt' => 'attendance_day29'],
	['db' => 'attendance_day30', 'dt' => 'attendance_day30'],
	['db' => 'attendance_day31', 'dt' => 'attendance_day31'],
	['db' => 'class_room', 'dt' => 'class_room'],
	['db' => 'student_name', 'dt' => 'student_name'],
	['db' => 'period_id', 'dt' => 'period_id'],
];

// List Validasi
$validation = $v->make($_POST, [
	'attendance_year'  => 'required|min:4',
	'attendance_month'  => 'required|min:2',
	'student_id'  => 'required',
	'attendance_count_s'  => 'required',
	'attendance_count_i'  => 'required',
	'attendance_count_a'  => 'required',
	'class_id'  => 'required',
	'period_id'  => 'required',
	'teacher_id'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'attendance_year'  => 'Kode Jadwal Pelajaran',
	'attendance_month'  => 'Nama Mata Pelajaran',
	'student_id'  => 'Hari [Senin s/d Minggu]',
	'attendance_count_s'  => 'Jam Mulai',
	'attendance_count_i'  => 'Jam Selesai',
	'attendance_count_a'  => 'Jam Selesai',
	'class_id'  => 'Kelas',
	'period_id'  => 'Periode',
	'teacher_id'  => 'Guru',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;

	// Cari Data
	case 'listSiswa':
	// Cetak Data Json
	echo json_encode(
		[
			"data" => $db->select("student_id, student_name")->table("student")->where( "class_id", (int)@$_GET['class_id'] )->getAll()
		]
	);
	break;

	// Proses Kelola Data
	case 'dataAbsensi':

	$period_id 	= (int)@$_POST['period_id'];
	$class_id 	= (int)@$_POST['class_id'];
	$tanggal 		= @$_POST['tanggal'];

	$data = explode("-",$tanggal);

	$statusDate = checkdate(@$data[1], @$data[2], @$data[0]);

	if (!$statusDate) {
		$tanggal = "";
		$param = "*";		
	} else {
		$param = "attendance.student_id, attendance.attendance_day" . (@$data[2]) . " AS data";		
	}

	// Cetak Data Json
	echo json_encode(
		[
			"data" => $db->select($param)->table("attendance")->join('student','attendance.student_id','student.student_id')->where( "period_id = ? AND student.class_id = ? AND attendance_year = ? AND attendance_month = ?",[$period_id, $class_id, $data[0], $data[1]])->getAll()
		]
	);
	break;

	// Proses Update Data
	case 'update':
	try {
		$year = date('Y', strtotime($_POST['attendance_day']));
		$month = date('m', strtotime($_POST['attendance_day']));
		$day = date('d', strtotime($_POST['attendance_day']));
		$dataSiswa = array();
		if ((int)$day>0 && (int)$day<32 && @$_POST['status']) {
			foreach ($_POST['status'] as $key => $value) {

				$cekAbsensi = $db->select("attendance_id, attendance_count_s, attendance_count_i, attendance_count_a, attendance_day$day")
				->table($table)
				->where("period_id", (int)$_POST['period_id'])
				->where("attendance_year", $year)
				->where("attendance_month", $month)
				->where("student_id", $key)
				->get();

				$statusNya = $value=="M" ? "A" : $value;
				// $cekAbsensi = $DataAbsen ? true : false;
				if ($cekAbsensi) {
					$attendance_count_sOld = $cekAbsensi->attendance_count_s;
					$attendance_count_iOld = $cekAbsensi->attendance_count_i;
					$attendance_count_aOld = $cekAbsensi->attendance_count_a;

					$attendance_count_s = $statusNya=="S" ? 1 : 0;
					$attendance_count_i = $statusNya=="I" ? 1 : 0;
					$attendance_count_a = $statusNya=="A" ? 1 : 0;

					$Absensi = json_decode($cekAbsensi->{"attendance_day$day"});
					// echo json_encode($Absensi);
					if ($Absensi!=null) {
						// echo "Ada Data";
						$statusNyaOld = $Absensi->status=="M" ? "A" : $Absensi->status;
						if ($statusNyaOld!=$statusNya) {
							$attendance_count_s = $statusNyaOld=="S" ? ($attendance_count_s-1) : $attendance_count_s;
							$attendance_count_i = $statusNyaOld=="I" ? ($attendance_count_i-1) : $attendance_count_i;
							$attendance_count_a = $statusNyaOld=="A" ? ($attendance_count_a-1) : $attendance_count_a;
						} else {
							$attendance_count_s = 0;
							$attendance_count_i = 0;
							$attendance_count_a = 0;
						}
						$Absensi->status = $value;

						if($_SESSION['Level']==1){
							$DataLog = (array)$Absensi->log->admin;
							array_push($DataLog, [
								"admin_id" => $_SESSION['UserID'],
								"datetime" => date("Y-m-d H:i:s"),
								"status" 	 => $value,
							]);
							$Absensi->log->admin = (object)$DataLog;
						} else  {
							$DataLog = (array)$Absensi->log->guru;
							array_push($DataLog, [
								"teacher_id" => $_SESSION['UserID'],
								"datetime" => date("Y-m-d H:i:s"),
								"status" 	 => $value,
							]);
							$Absensi->log->admin = (object)$DataLog;
						}
					} else {
						// echo "Tidak Ada Data";
						if($_SESSION['Level']==1){
							$Absensi = [
								"status" => $value,
								"log" => [
									"admin" => (array)[
										0 => [
											"admin_id" => $_SESSION['UserID'],
											"datetime" => date("Y-m-d H:i:s"),
											"status" 	 => $value,
										]
									],
									"guru" => [],
								],
							];
						} else  {
							$Absensi = [
								"status" => $value,
								"log" => [
									"admin" => [],
									"guru" => (array)[
										0 => [
											"teacher_id" => $_SESSION['UserID'],
											"datetime" => date("Y-m-d H:i:s"),
											"status" 	 => $value,
										]
									],
								],
							];
						}
					}

					$dataAbsensi = [
						'period_id' => (int)$_POST['period_id'],
						'attendance_year' => $year,
						'attendance_month' => $month,
						'student_id' => $key,
						'attendance_count_s' => $attendance_count_sOld + $attendance_count_s,
						'attendance_count_i' => $attendance_count_iOld + $attendance_count_i,
						'attendance_count_a' => $attendance_count_aOld + $attendance_count_a,
						"attendance_day$day" => json_encode($Absensi),
					];

					$dataSiswa[] = $dataAbsensi;
					$db->table($table)
					->where("attendance_id", $cekAbsensi->attendance_id)
					->update($dataAbsensi);
				} else {

					$attendance_count_s = $statusNya=="S" ? 1 : 0;
					$attendance_count_i = $statusNya=="I" ? 1 : 0;
					$attendance_count_a = $statusNya=="A" ? 1 : 0;

					if($_SESSION['Level']==1){
						$Absensi = [
							"status" => $value,
							"log" => [
								"admin" => (array)[
									0 => [
										"admin_id" => $_SESSION['UserID'],
										"datetime" => date("Y-m-d H:i:s"),
										"status" 	 => $value,
									]
								],
								"guru" => [],
							],
						];
					} else  {
						$Absensi = [
							"status" => $value,
							"log" => [
								"admin" => [],
								"guru" => (array)[
									0 => [
										"teacher_id" => $_SESSION['UserID'],
										"datetime" => date("Y-m-d H:i:s"),
										"status" 	 => $value,
									]
								],
							],
						];
					}

					$dataAbsensi = [
						'period_id' => (int)$_POST['period_id'],
						'class_id' => (int)$_POST['class_id'],
						'attendance_year' => $year,
						'attendance_month' => $month,
						'student_id' => $key,
						'attendance_count_s' => $attendance_count_s,
						'attendance_count_i' => $attendance_count_i,
						'attendance_count_a' => $attendance_count_a,
						"attendance_day$day" => json_encode($Absensi),
					];
					// $dataSiswa[] = $dataAbsensi;
					$dataSiswa[] = $db->table($table)
					->insert($dataAbsensi);

				}
			}
			$status = false;
		} else {
			$status = true;
		}
		// $db->table($table)->where($primaryKey, $id)->update($validData);
		// $status =  $db->commit();

	}catch(PDOException $e){
			// echo $e->getMessage();
		$status = true;
	}
	// Cetak Data Json
	echo json_encode(['error'=>$status,'data'=>$dataSiswa]);
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}