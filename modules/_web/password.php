<div class="page bg-light height-full">
  <header class="blue accent-3 relative">
    <div class="container-fluid text-white">
      <div class="row justify-content-between">
        <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
          <li>
            <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#ubahPassword"
            role="tab" aria-controls="v-pills-all"><i class="icon icon-vpn_key"></i>Ubah Password</a>
          </li>
        </ul>
      </div>
    </div>
  </header>

  <div class="container-fluid animatedParent animateOnce">
    <div class="tab-content my-3" id="v-pills-tabContent">
      <div class="tab-pane animated fadeInUpShort show active" id="ubahPassword" role="tabpanel" aria-labelledby="v-pills-all-tab">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header white">
                Masukkan Password Lama & Baru Anda Untuk Mengubah Password
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="card-text"></div>
                  <form class="form" action="api.php?p=password" method="post">
                    <?php echo csrf_field() ?>
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="passwordLama">Password Lama </label>
                            <input id="passwordLama" class="form-control r-0 light s-12 " placeholder="Masukkan Password Lama" name="password" type="password" required="">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="passwordBaru">Password Baru </label>
                            <input id="passwordBaru" class="form-control r-0 light s-12 " placeholder="Masukkan Password Baru" name="newpassword" type="password" required="" minlength="8">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="CNewPass">Konfirmasi Password Baru</label>
                            <input id="CNewPass" class="form-control r-0 light s-12 " placeholder="Masukkan Ulang Password Baru" name="cnewpassword" type="password" required="" minlength="8">
                          </div>
                        </div>
                        <hr>
                      </div>
                    </div>
                    <div class="form-actions">
                      <hr>
                      <a href="javascript:window.history.back();" class="btn btn-danger btn-sm mr-1">
                        <i class="icon-arrow_back mr-2"></i> Kembali
                      </a>
                      <button type="submit" class="btn btn-success btn-sm mr-1 tombol-simpan">
                        <i class="icon-save mr-2"></i> Simpan
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  window.onload = function() {

    $('#menu-name').html('Pengaturan Sandi');
    $('.sidebar-menu').find('.menu-status').removeClass('active');
    $('#userSettingsCollapse').addClass('show');
    $('#password').addClass('active');

    $(".tombol-simpan").click(function(){
      var data = $('.form').serialize();
      $.ajax({
        type: 'POST',
        url: "api.php?p=password",
        data: data,
        success: function(data) {
          data = $.parseJSON(data);
          $('input[name="_token"]').val(data.message.csrf_token.newToken);

          if(data.error==false){
            $type='success';
            $message = 'Berhasil Ubah Password';
          } else {
            $type='error';
            $message = 'Ubah Password Gagal';
          }

          $info = '';
          $.each(data.message.form, function(index, val) {
            $info += (index+1) + ". " + val + "<br>";
          });

          Swal.fire({
            type: $type,
            title: $message,
            html: $info,
            timer: 1500 * (data.message.form.length + 1)
          });

          $('.form').trigger("reset");
          $('.form').trigger("change");
        }
      });
    });
  }
</script>

