<script type="text/javascript">
	window.onload = function() {
		$('#menu-name').html(menuName);
		$('.sidebar-menu').find('.menu-status').removeClass('active');
		$('.sidebar-menu').find(`[data-menu="${menu}"]`).addClass('active');

		var table = $('#dataTable-SS').DataTable({
			"bProcessing": true,
			"language": {
				"decimal":        ",",
				"emptyTable":     "Data Tidak Tersedia...",
				"info":           "Tampil _START_ - _END_ dari _TOTAL_ Data",
				"infoEmpty":      "Tampil 0 to 0 of 0 Data",
				"infoFiltered":   "(filter dari _MAX_ total Data)",
				"infoPostFix":    "",
				"thousands":      ".",
				"lengthMenu":     "Tampil _MENU_ Data",
				"loadingRecords": "Loading...",
				"processing":     "Memperbarui Data",
				"searchPlaceholder": "Ketik Untuk Cari Data ...",
				"search":         "",
				"zeroRecords":    "Data Tidak Ditemukan",
				"paginate": {
					"first":      "Awal",
					"last":       "Akhir",
					"next":       "Lanjut",
					"previous":   "Kembali"
				}
			},
			"autoWidth": false,
			"ajax": {
				"url": apiPath + "&act=datatables",
				"type": "POST",
				data: function ( d ) {
					return $.extend( {}, d, {
						"product_type_id": $("#form-product_type_id").val(),
						"product_id": $("#form-product_id").val(),
						"unit_id": $("#form-unit_id").val(),
						"date_start" : $("#form-date_start").val(),
						"date_end" : $("#form-date_end").val(),
					} );
				}

			},
			"bServerSide": true,
			"scrollX" : true,
			"scrollCollapse" : true,
			"aoColumns" : DtaoColumns,
			"columnDefs": DtcolumnDefs,
			"initComplete": function(settings, json) {
        // Tindakan Setelah Datatables Selesai Dijalankan
    }
});

    // Aksi Simpan Perubahan Data
    $(".tombol-tambah").click(function(){

    	let $form = $("#TambahForm");
    	let data = $form.serialize();

    	$.ajax({
    		type: 'POST',
    		url:apiPath + `&act=create`,
    		data: data,
    		success: function(data) {

    			$('input[name="_token"]').val(data.csrf_token);

    			if(data.error==false){
    				$type='success';
    				$message = 'Tambah Data Berhasil';

    				$('.removeForm').trigger('click');
    				getData();
    			} else {
    				$type='error';
    				$message = 'Tambah Data Gagal';
    			}

    			$info = '';
    			$.each(data.messages.form, function(index, val) {
    				$info += (index+1) + ". " + val + "<br>";
    			});

    			Swal.fire({
    				type: $type,
    				title: $message,
    				html: $info,
    				timer: 1500 * (data.messages.form.length + 1)
    			});
    		}
    	});
    });

    // Aksi Open Form Edit
    $(document).on('click','[data-edit]',function(){

    	let $param = $(this);
    	let editId = $param.data('edit');

    	$.get(apiPath + `&act=read&${idKey}=`+editId, function(data) {
    		$.each(data, function(index, val) {
    			$('#edit-'+index).val(val).trigger('change');
    		});

    		if (typeof customUpdateForm === 'function') {
    			customUpdateForm(data);
    		}
    	});
    	$('#edit-tab').show();
    	$('a[href="#form-edit"]').trigger('click');
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-edit").click(function(){

    	let $form = $("#EditForm");
    	let data = getFormData($form);
    	let editId = data[idKey];
    	data = $form.serialize();

    	$.ajax({
    		type: 'POST',
    		url:apiPath + `&act=update&${idKey}=`+editId,
    		data: data,
    		success: function(data) {

    			$('input[name="_token"]').val(data.csrf_token);

    			if(data.error==false){
    				$type='success';
    				$message = 'Ubah Data Berhasil';

    				$('.removeForm').trigger('click');
    				getData();
    			} else {
    				$type='error';
    				$message = 'Ubah Data Gagal';
    			}

    			$info = '';
    			$.each(data.messages.form, function(index, val) {
    				$info += (index+1) + ". " + val + "<br>";
    			});

    			Swal.fire({
    				type: $type,
    				title: $message,
    				html: $info,
    				timer: 1500 * (data.messages.form.length + 1)
    			});
    		}
    	});
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
    	$('#edit-tab').hide();
    	$('a[href="#data"]').trigger('click');
    	$('.form').trigger("reset");
    	$('.form').trigger("change");
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
    	let $param = $(this);
    	let deleteId = $param.data('del');

    	Swal.fire({
    		title: "Apakah Anda Yakin Menghapus Ingin Data?",
    		text: "Jika Yakin, Silahkan Pilih Ya!",
    		type: 'warning',
    		showCancelButton: true,
    		confirmButtonText: 'Ya, Hapus Sekarang',
    		cancelButtonText: 'Tidak, Batalkan!',
    		confirmButtonColor: '#d33',
    		cancelButtonColor: '#333',
    	}).then(isConfirm =>
    	{
    		if (isConfirm.value==true) {
    			$.ajax({
    				type: 'GET',
    				url: apiPath + "&act=delete",
    				data: `${idKey}=`+deleteId,
    				success: function(msg){
    					Swal.fire("Terhapus!", msg, "success");
    					getData();
    				},
    				error: function (request, msg, error) {
    					Swal.fire("Terjadi Kesalahan", request.responseText, "error");
    				}
    			});
    		} else {
    			Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
    		}
    	})
    });

    const activityWatcher = () => {

    	let secondsSinceLastActivity = 0;

    	let maxInactivity = 10;

    	setInterval(function(){
    		secondsSinceLastActivity++;
    		if(secondsSinceLastActivity >= maxInactivity){
    			(secondsSinceLastActivity%10)==0 ? getData() : '';
    		}
    	}, 100000);

    	function activity(){
    		secondsSinceLastActivity = 0;
    	}

    	var activityEvents = [
    	'mousedown', 'mousemove', 'keydown',
    	'scroll', 'touchstart'
    	];

    	activityEvents.forEach(function(eventName) {
    		document.addEventListener(eventName, activity, true);
    	});
    }

    activityWatcher();

    const getData = () => {
    	table.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    $('.reload-data').click(function(){
    	getData();
    });

    $('#reload-data').click(function(){
    	getData();
    });

    $("#tambah-unit_id").select2({
    	ajax: {
    		url: "api.php?p=unit&act=select2",
    		dataType: 'json'
    	}
    });

    $("#edit-unit_id").select2({
    	ajax: {
    		url: "api.php?p=unit&act=select2",
    		dataType: 'json'
    	}
    });

    $("#tambah-product_type_id").select2({
    	ajax: {
    		url: "api.php?p=product_type&act=select2",
    		dataType: 'json'
    	}
    });

    $("#edit-product_type_id").select2({
    	ajax: {
    		url: "api.php?p=product_type&act=select2",
    		dataType: 'json'
    	}
    });

    $("#form-product_type_id").select2({
    	ajax: {
    		url: "api.php?p=product_type&act=select2",
    		dataType: 'json'
    	}
    });

    $("#tambah-product_id").select2({
    	ajax: {
    		url: "api.php?p=product&act=select2",
    		dataType: 'json'
    	}
    });

    $("#edit-product_id").select2({
    	ajax: {
    		url: "api.php?p=product&act=select2",
    		dataType: 'json'
    	}
    });

    $("#form-product_id").select2({
    	ajax: {
    		url: "api.php?p=product&act=select2",
    		dataType: 'json',
    		data: function (params) {
    			var query = {
    				q : params.term,
    				product_type_id : $("#form-product_type_id").val()
    			}
    			return query;
    		}
    	}
    });
}
</script>