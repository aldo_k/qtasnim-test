    <aside class="main-sidebar fixed offcanvas shadow">
      <section class="sidebar">
        <div class="mt-3 mb-3 ml-3">
          <!-- <img src="logo.png" alt=""> -->
          <h5>PT. Qtasnim Digital Teknologi</h5>
        </div>
        <div class="relative">
          <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
            <i class="icon icon-cogs"></i>
          </a>
          <div class="user-panel p-3 light mb-2">
            <div>
              <div class="float-left image">
                <img class="user_avatar" src="logo.png" alt="User Image">
              </div>
              <div class="float-left info">
                <h6 class="font-weight-light mt-2 mb-1"><?= $_SESSION['Fullname'] ?></h6>
                <i class="icon-circle text-success blink"></i> Online
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="collapse multi-collapse" id="userSettingsCollapse">
              <div class="list-group mt-3 shadow">
                <a href="?p=password" class="list-group-item list-group-item-action" id="password">
                  <i
                  class="mr-2 icon-vpn_key text-yellow"></i>Ubah Password
                </a>
                <a href="auth.php?p=logout" class="list-group-item list-group-item-action"><i
                  class="mr-2 icon-sign-out text-red"></i>Logout (Keluar)
                </a>
              </div>
            </div>
          </div>
        </div>
        <ul class="sidebar-menu">
          <li class="header"><strong>MAIN NAVIGATION</strong></li>
          <li class="menu-status" data-menu="beranda">
            <a href="?p=dashboard">
              <i class="icon icon-home2 s-18"></i> <span>Beranda</span>
            </a>
          </li>
          <li class="menu-status" data-menu="transaction">
            <a href="?p=transaction">
              <i class="icon icon-show_chart s-18"></i> <span>Transaksi Penjualan</span>
            </a>
          </li>
          <?php if($_SESSION['Level']==1){ ?>
            <li class="header light mt-3"><strong>Data Master</strong></li>
            <li class="menu-status" data-menu="admin">
              <a href="?p=admin">
                <i class="icon icon-supervisor_account s-18"></i> <span>Akun Pengguna</span>
              </a>
            </li>
            <li class="menu-status" data-menu="product">
              <a href="?p=product">
                <i class="icon icon icon-package s-18"></i> <span>Barang</span>
              </a>
            </li>
            <li class="menu-status" data-menu="product_type">
              <a href="?p=product_type">
                <i class="icon icon-th-large s-18"></i> <span>Jenis Barang</span>
              </a>
            </li>
            <li class="menu-status" data-menu="unit">
              <a href="?p=unit">
                <i class="icon icon-th s-18"></i> <span>Satuan (UoM)</span>
              </a>
            </li>
          <?php } ?>
        </ul>
      </section>
    </aside>
    <!--Sidebar End-->
    <div class="page has-sidebar-left">
      <div class="navbar navbar-expand d-flex navbar-dark justify-content-between bd-navbar blue accent-3 shadow">
        <div class="relative">
          <div class="d-flex">
            <div>
              <a href="#" data-toggle="offcanvas" class="paper-nav-toggle pp-nav-toggle">
                <i></i>
              </a>
            </div>
            <div class="d-none d-md-block">
              <h1 class="nav-title text-white" id="menu-name"></h1>
            </div>
          </div>
        </div>
        <!--Top Menu Start -->
        <div class="navbar-custom-menu p-t-10">
          <ul class="nav navbar-nav">
            <li class="dropdown custom-dropdown user user-menu notifications-menu">
              <a href="#" class="nav-link" data-toggle="dropdown">
                <img src="logo.png" class="user-image" alt="User Image">
                <i class="icon-more_vert "></i>
              </a>
              <ul class="dropdown-menu" style="width:180px">
                <li class="header">Hai, <?= $_SESSION['Fullname'] ?></li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="?p=password">
                        <i class="icon icon-vpn_key"></i> Ubah Password
                      </a>
                    </li>
                    <li>
                      <a href="auth.php?p=logout">
                        <i class="icon icon-sign-out"></i> Logout (Keluar)
                      </a>
                    </li>
                  </ul>
                </li>
                <!-- <li class="footer p-2 text-center"><a href="#">View all</a></li> -->
              </ul>
            </li>
          </ul>
        </div>
      </div>
