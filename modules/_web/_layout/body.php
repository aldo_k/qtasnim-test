<div class="container-fluid animatedParent animateOnce my-3">
	<div class="animated fadeInUpShort">
		<div class="card">
			<div class="card-header white">
				<h6> Soal Test PT. Qtasnim Digital Teknologi </h6>
			</div>
			<div class="card-body">
				<div class="row text-justify">
					<div class="col-sm-6">
						<h3 class="text-center">Salam Qtasnim</h3>
						<p>

							Assalamu’alaikum Wr. Wb.

							Puji syukur atas rahmat – berkah Allah S.W.T dalam proses perjalanan kami memulai Qtasnim sampai di hari ini. Solawat serta salam kepada junjungan Nabi Muhammad S.A.W yang menjadi landasan dan rujukan kami dalam menjalani perjalanan hidup ini.

							Terima kasih kepada tim terbaik yang masih menjadi bagian Qtasnim.

							Terima kasih kepada mereka, para stakeholder yang selalu mempercayai dalam proses perjalanan Qtasnim sejak tahun 2012.

							Bagi kami, klien adalah aset terpenting setelah internal Qtasnim, terima kasih atas kepercayaan mereka bekerja dengan kami.

							Qtasnim hadir sebagai solusi teknologi bagi setiap organisasi bisnis – individu yang membutuhkan jasa – produk teknologi informasi. Konsistensi dengan terus mempertahankan kualitas kami adalah kewajiban Qtasnim.

							Terakhir tidak ada upaya untuk penyelesaian masalah melainkan dengan tetap menjaga kesungguhan yang sesungguh-sungguhnya, ketahanan, keuletan dan konsistensi dengan semangat untuk menyelesaikan masalah.

							Dengan terus menambah nilai (ugrade) internal Qtasnim serta pelayanan yang optimal, diharapkan dapat menjawab permasalahan yang dihadapi.

							Saya yakin dengan sikap yang baik dan bertanggung jawab Insya Allah tidak ada masalah yang sulit, karena setiap kesulitan akan ada kemudahan jika kita menjaga rasa tanggung jawab.

							Wassalamualaikum Wr. Wb.
						</p>
					</div>
					<div class="col-sm-6">
						<h3 class="text-center">Perjalanan Qtasnim</h3>
						<p>
							PT. Qtasnim Digital Teknologi (Qtasnim) berdiri secara legal sejak Mei 2013. Qtasnim merupakan perusahaan yang berkomitmen bergerak di bidang solusi dan pelayanan bidang teknologi informasi. Tim kami merupakan para profesional yang telah memiliki dedikasi dan pengalaman di bidang teknologi informasi lebih dari 7 tahun.​

							Kami berpengalaman dalam memberikan solusi terbaik dalam membangun dan mengembangkan aplikasi enterprise, integrasi sistem, pengembangan webportal dan aplikasi mobile. Soliditas dan integritas tim telah mampu membangun perusahaan yang unggul dan dapat dipertanggungjawabkan untuk memberikan layanan terbaik bagi klien dan seluruh stakeholder terkait dalam bisnis dan pengembangannya. Oleh karenanya, kualitas telah menjadi komitmen kami.​

						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
