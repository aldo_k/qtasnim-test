<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Satuan (UoM)</a>
					</li>
					<li>
						<a class="nav-link " id="v-pills-all-tab" data-toggle="pill" href="#form-tambah"
						role="tab" aria-controls="form-tambah"><i class="icon icon-plus-circle"></i>Tambah Satuan (UoM)</a>
					</li>
          <li id="edit-tab" style="display: none;">
            <a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#form-edit" role="tab"
            aria-controls="form-edit"><i class="icon icon-edit"></i> Edit Data</a>
          </li>
          <!--  <li class="float-right">
            <a class="nav-link"  href="panel-page-users-create.html" ><i class="icon icon-plus-circle"></i> Add New User</a>
          </li> -->
        </ul>
      </div>
    </div>
  </header>
  <!-- Start Tab Content -->
  <div class="container-fluid animatedParent animateOnce">
  	<div class="tab-content my-3" id="v-pills-tabContent">
      <!-- Tab View Data Start -->
      <div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
       <div class="row">
        <div class="col-md-12">
         <div class="card">
          <div class="card-header white">
            <i class="icon-list-alt blue-text"></i>
            <strong> Data Satuan (UoM) </strong>
            <button type="button" id="reload-data" class="btn btn-xs btn-primary r-5 float-right"><i class="icon-refresh"></i> Reload Data</button>
          </div>
          <div class="card-body">
            <div class="card-title"></div>
            <table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab View Data End -->

  <!-- Tab Tambah Data Start -->
  <div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header white">
            Tambah Satuan (UoM) Baru
          </div>
          <div class="card-content">
            <div class="card-body">
              <div class="card-text" id="notification-tambah"></div>
              <form id="TambahForm" class="form" action="javascript:void(0);" method="post">
                <?php echo csrf_field() ?>
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-alias">Alias Satuan (UoM) </label>
                        <input id="tambah-alias" class="form-control r-0 light s-12 " placeholder="Masukkan Alias Satuan (UoM)" name="alias" type="text" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-unit">Nama Satuan (UoM) </label>
                        <input id="tambah-unit" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Satuan (UoM)" name="unit" type="text" required="">
                      </div>
                    </div>

                    <hr>
                  </div>
                </div>
                <div class="form-actions">
                  <hr>
                  <button class="btn btn-danger btn-sm mr-1 removeForm">
                    <i class="icon-arrow_back mr-2"></i> Kembali
                  </button>
                  <button type="button" class="btn btn-success btn-sm mr-1 tombol-tambah">
                    <i class="icon-save mr-2"></i> Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab Tambah Data End -->

  <!-- Tab Edit Data Start-->
  <div class="tab-pane animated fadeInUpShort" id="form-edit" role="tabpanel" aria-labelledby="v-pills-all-tab">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header white">
            Ubah
          </div>
          <div class="card-content">
            <div class="card-body">
              <div class="card-text" id="notification-edit"></div>
              <form id="EditForm" class="form" action="javascript:void(0);" method="post">
                <?php echo csrf_field() ?>
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-alias">Alias Satuan (UoM) </label>
                        <input id="edit-id" type="hidden" name="id">
                        <input id="edit-alias" class="form-control r-0 light s-12 " placeholder="Masukkan Alias Satuan (UoM)" name="alias" type="text" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-unit">Nama Satuan (UoM) </label>
                        <input id="edit-unit" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Satuan (UoM)" name="unit" type="text" required="">
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div class="form-actions">
                  <hr>
                  <button type="button" class="btn btn-danger btn-sm mr-1 removeForm">
                    <i class="icon-arrow_back mr-2"></i> Kembali
                  </button>
                  <button type="button" class="btn btn-success btn-sm mr-1 tombol-edit">
                    <i class="icon-save mr-2"></i> Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab Edit Data End-->
</div>
</div>
<!-- End Tab Content -->
</div>
<script type="text/javascript">
    var idKey   = 'id';
    var menuName = 'Satuan (UoM)';
    var menu = 'unit';
    var apiPath = 'api.php?p=unit';

    // Definisi Kolom Table
    DtaoColumns = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
      return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "alias", "title": "Alias Satuan (UoM)", "name": "alias" },
    { "data": "unit", "title": "Nama Satuan (UoM)", "name": "unit" },
    { "data": idKey, "title": "Tindakan", "name": idKey, "render": function ( data, type, full ) {
      return '<a data-edit="'+full[idKey]+'" class="btn btn-sm btn-warning text-white" title="Ubah Data"><i class="icon-edit"></i></a> <button data-del="'+full[idKey]+'" data-load="" class="btn btn-sm btn-danger text-white" title="Hapus Data"><i class="icon-trash"></i></button>';
    }}
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    { width: "50px", targets: 3 },
    ];
</script>
<?php
require(__DIR__ . "/_layout/default_js.php");
