<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Transaksi Penjualan</a>
					</li>
					<li>
						<a class="nav-link " id="v-pills-all-tab" data-toggle="pill" href="#form-tambah"
						role="tab" aria-controls="form-tambah"><i class="icon icon-plus-circle"></i>Tambah Transaksi Penjualan</a>
					</li>
					<li id="edit-tab" style="display: none;">
						<a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#form-edit" role="tab"
						aria-controls="form-edit"><i class="icon icon-edit"></i> Edit Data</a>
					</li>
          <!--  <li class="float-right">
            <a class="nav-link"  href="panel-page-users-create.html" ><i class="icon icon-plus-circle"></i> Add New User</a>
        </li> -->
    </ul>
</div>
</div>
</header>
<!-- Start Tab Content -->
<div class="container-fluid animatedParent animateOnce">
	<div class="tab-content my-3" id="v-pills-tabContent">
		<!-- Tab View Data Start -->
		<div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header white">
							<i class="icon-list-alt blue-text"></i>
							<strong> Data Transaksi Penjualan </strong>
							<button type="button" id="reload-data" class="btn btn-xs btn-primary r-5 float-right"><i class="icon-refresh"></i> Reload Data</button>
						</div>
						<div class="card-body">
							<div class="card-title">
								<div class="row form-inline">
									<div class="col-sm-12">
										<form class="form-group">
											<div class="col-sm-2">
												<select id="form-product_type_id" class="form-control r-0 light s-12" name="product_type_id" required="">
													<option value="">Semua Jenis Barang </option>
												</select>
											</div>
											<div class="col-sm-2">
												<select id="form-product_id" class="form-control r-0 light s-12" name="product_id" required="">
													<option value="">Semua Barang </option>
												</select>
											</div>
											<div class="col-sm-1">
												Filter Tanggal :
											</div>
											<div class="col-sm-2">
												<input id="form-date_start" type="text" class="date-time-picker form-control r-0 light s-12" name="date_start" placeholder="Tanggal Awal" data-options='{"timepicker":false, "format":"Y-m-d"}' value="<?= date('Y-m-d'); ?>">
											</div>
											<div class="col-sm-1">
												s/d
											</div>
											<div class="col-sm-2">
												<input id="form-date_end" type="text" class="date-time-picker form-control r-0 light s-12" name="date_end" placeholder="Tanggal Akhir" data-options='{"timepicker":false, "format":"Y-m-d"}' value="<?= date('Y-m-d'); ?>">
											</div>
											<div class="col-sm-2">
												<button type="button" class="btn btn-sm btn-primary mr-2 r-5 reload-data"><i class="icon icon-search"></i>Filter</button>
											</div>
										</form>
									</div>
								</div>
								<hr>
							</div>
							<table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Tab View Data End -->

		<!-- Tab Tambah Data Start -->
		<div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header white">
							Tambah Transaksi Penjualan Baru
						</div>
						<div class="card-content">
							<div class="card-body">
								<div class="card-text" id="notification-tambah"></div>
								<form id="TambahForm" class="form" action="javascript:void(0);" method="post">
									<?php echo csrf_field() ?>
									<div class="form-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="tambah-transaction_date">Nama Transaksi Penjualan </label>
													<input id="tambah-transaction_date" type="text" class="date-time-picker form-control r-0 light s-12" name="transaction_date" placeholder="Tanggal Akhir" data-options='{"timepicker":false, "format":"Y-m-d"}' value="<?= date('Y-m-d'); ?>">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="tambah-product_id">Barang </label>
													<select id="tambah-product_id" class="form-control r-0 light s-12" name="product_id" required="">
														<option value="">Silahkan Pilih Barang</option>
													</select>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="tambah-qty">Jumlah Barang </label>
													<input id="tambah-qty" class="form-control r-0 light s-12 " placeholder="Masukkan Jumlah Barang" name="qty" type="number" min="0" required="">
												</div>
											</div>
											<hr>
										</div>
									</div>
									<div class="form-actions">
										<hr>
										<button class="btn btn-danger btn-sm mr-1 removeForm">
											<i class="icon-arrow_back mr-2"></i> Kembali
										</button>
										<button type="button" class="btn btn-success btn-sm mr-1 tombol-tambah">
											<i class="icon-save mr-2"></i> Simpan
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Tab Tambah Data End -->

		<!-- Tab Edit Data Start-->
		<div class="tab-pane animated fadeInUpShort" id="form-edit" role="tabpanel" aria-labelledby="v-pills-all-tab">
			<div class="row">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header white">
							Ubah
						</div>
						<div class="card-content">
							<div class="card-body">
								<div class="card-text" id="notification-edit"></div>
								<form id="EditForm" class="form" action="javascript:void(0);" method="post">
									<?php echo csrf_field() ?>
									<div class="form-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="edit-transaction_date">Tanggal Transaksi Penjualan </label>
													<input id="edit-id" type="hidden" name="id">
													<input id="edit-transaction_date" type="text" class="date-time-picker form-control r-0 light s-12" name="transaction_date" placeholder="Tanggal Akhir" data-options='{"timepicker":false, "format":"Y-m-d"}' value="<?= date('Y-m-d'); ?>">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="edit-product_id">Barang </label>
													<select id="edit-product_id" class="form-control r-0 light s-12" name="product_id" required="">
														<option value="">Silahkan Pilih Barang</option>
													</select>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="edit-qty">Jumlah Barang </label>
													<input id="edit-qty" class="form-control r-0 light s-12 " placeholder="Masukkan Jumlah Barang" name="qty" type="number" min="0" required="">
												</div>
											</div>
											<hr>
										</div>
									</div>
									<div class="form-actions">
										<hr>
										<button type="button" class="btn btn-danger btn-sm mr-1 removeForm">
											<i class="icon-arrow_back mr-2"></i> Kembali
										</button>
										<button type="button" class="btn btn-success btn-sm mr-1 tombol-edit">
											<i class="icon-save mr-2"></i> Simpan
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Tab Edit Data End-->
	</div>
</div>
<!-- End Tab Content -->
</div>
<script type="text/javascript">
	var idKey   = 'id';
	var menuName = 'Transaksi Penjualan';
	var menu = 'transaction';
	var apiPath = 'api.php?p=transaction';

    // Definisi Kolom Table
    DtaoColumns = [
    { "data": 'trx.id', "title": "No", "name": 'id',"render": function ( data, type, row, meta ) {
    	return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "product", "title": "Barang", "name": "product" },
    { "data": "stock_old", "title": "Stok", "name": "stock_old" },
    { "data": "qty", "title": "Jumlah Terjual", "name": "qty" },
    { "data": "transaction_date", "title": "Tanggal Transaksi", "name": "transaction_date" },
    { "data": "product_type", "title": "Jenis Barang", "name": "product_type" },
    { "data": idKey, "title": "Tindakan", "name": idKey, "render": function ( data, type, full ) {
    	return '<a data-edit="'+full[idKey]+'" class="btn btn-sm btn-warning text-white" title="Ubah Data"><i class="icon-edit"></i></a> <button data-del="'+full[idKey]+'" data-load="" class="btn btn-sm btn-danger text-white" title="Hapus Data"><i class="icon-trash"></i></button>';
    }}
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    { width: "50px", targets: 6 },
    ];

    const customUpdateForm = (data) => {
    	let newOptionType = new Option(`${data.product}`, data.product_id);
    	$('#edit-product_id').append(newOptionType).trigger('change');
    	$('#edit-product_id').val(data.product_id).trigger('change');
    }
</script>
<?php
require(__DIR__ . "/_layout/default_js.php");
