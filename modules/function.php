<?php
// https://github.com/banujan6/CSRF-handler
use csrfhandler\csrf as csrf;

$isValidToken = csrf::post() || @$_POST['type']=='api' || @$_GET['type']=='api'; // return TRUE or FALSE

if (!function_exists('response')) {
	function response($data = null, $messages = "OK")
	{
		return json_encode([
			'error'      => false,
			'messages'   => $messages,
			'data'       => $data,
			'csrf_token' => csrf_token(),
		]);
	}
}

if (!function_exists('response_error')) {
	function response_error($data = null, $messages = "Invalid Token!")
	{
		return json_encode([
			'error'      => true,
			'messages'   => $messages,
			'data'       => $data,
			'csrf_token' => csrf_token(),
		]);
	}
}

if (!function_exists('csrf_field')) {
	function csrf_field()
	{
		$token 	= csrf::token();
		return "<input type=\"hidden\" name=\"_token\" value=\"$token\">";
	}
}

if (!function_exists('csrf_token')) {
	function csrf_token()
	{
		return csrf::token();
	}
}

if (!function_exists('is_json')) {
	function is_json($string,$return_data = true) {
		$data = json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
	}
}

if (!function_exists('tgl_indo')) {
	function tgl_indo($param)
	{
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecah = explode('-', $tanggal);

		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecah[2] . ' ' . $bulan[ (int)$pecah[1] ] . ' ' . $pecah[0];
	}
}