-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2021 at 06:44 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qtasnim_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product` varchar(191) NOT NULL,
  `stock` decimal(12,2) NOT NULL DEFAULT 0.00,
  `stock_alert` decimal(12,2) NOT NULL DEFAULT 0.00,
  `product_type_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `descriptions` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product`, `stock`, `stock_alert`, `product_type_id`, `unit_id`, `descriptions`, `created_at`, `updated_at`) VALUES
(1, 'Kopi', '99.00', '10.00', 1, 4, '-', '2021-10-29 16:07:08', '2021-10-31 17:40:15'),
(2, 'Teh', '84.00', '10.00', 1, 4, '-', '2021-10-31 15:12:25', '2021-10-31 17:39:50'),
(3, 'Pasta Gigi', '100.00', '10.00', 2, 2, '-', '2021-10-31 15:13:04', '2021-10-31 15:13:04'),
(4, 'Sabun Mandi', '100.00', '10.00', 2, 2, '-', '2021-10-31 15:13:29', '2021-10-31 15:13:29'),
(5, 'Sampo', '95.00', '10.00', 2, 2, '-', '2021-10-31 15:14:56', '2021-10-31 17:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(11) NOT NULL,
  `alias` varchar(12) NOT NULL,
  `product_type` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `alias`, `product_type`, `created_at`, `updated_at`) VALUES
(1, 'K-1', 'Konsumsi', '2021-10-29 15:00:25', '2021-10-29 15:00:34'),
(2, 'K-2', 'Pembersih', '2021-10-31 15:11:49', '2021-10-31 15:11:49');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stock_old` decimal(12,2) NOT NULL,
  `qty` decimal(12,2) NOT NULL,
  `transaction_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `product_id`, `stock_old`, `qty`, `transaction_date`, `created_at`, `updated_at`) VALUES
(3, 2, '100.00', '13.00', '2021-11-01', '2021-10-31 17:39:26', '2021-10-31 17:39:26'),
(4, 2, '87.00', '3.00', '2021-11-01', '2021-10-31 17:39:50', '2021-10-31 17:39:50'),
(5, 5, '100.00', '5.00', '2021-11-01', '2021-10-31 17:40:00', '2021-10-31 17:40:00'),
(6, 1, '112.00', '13.00', '2021-11-01', '2021-10-31 17:40:08', '2021-10-31 17:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `alias` varchar(12) NOT NULL,
  `unit` varchar(191) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `alias`, `unit`, `created_at`, `updated_at`) VALUES
(2, 'Pcs', 'Pcs', '2021-10-29 14:52:11', '2021-10-29 14:52:11'),
(3, 'Cm', 'Centimeter', '2021-10-29 14:54:02', '2021-10-29 14:54:02'),
(4, 'Bks', 'Bungkus', '2021-10-29 16:05:28', '2021-10-29 16:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `fullname` varchar(128) NOT NULL,
  `username` varchar(18) NOT NULL,
  `password` varchar(128) NOT NULL,
  `users_type` tinyint(1) NOT NULL,
  `ref_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `fullname`, `username`, `password`, `users_type`, `ref_id`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '0'),
(140, 'Azka', '123', '202cb962ac59075b964b07152d234b70', 3, '123456'),
(141, '111', '111', '698d51a19d8a121ce581499d7b701668', 3, '111'),
(142, '222', '222', 'bcbe3365e6ac95ea2c0343a2395834dd', 3, '222'),
(143, '333', '333', '310dcbbf4cce62f762a2aaa148d556bd', 3, '333'),
(144, '444', '444', '550a141f12de6341fba65b0ad0433500', 3, '444'),
(145, 'Husni', 'husni', 'e10adc3949ba59abbe56e057f20f883e', 2, '7'),
(146, 'amante', '1111', 'b59c67bf196a4758191e42f76670ceba', 2, '8'),
(147, 'qqq', '33333', 'b7bc2a2f5bb6d521e64c8974c143e9a0', 3, '33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
